import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';


import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { initialState, reducers, effects } from './store/state/app.state';

import { BookmarkComponent } from './components/bookmark/bookmark.component';
import { PopupComponent } from './components/popup/popup.component';
import { MessageComponent } from './components/popup/popup.component';
import { BmTableComponent } from './components/bm-table/bm-table.component';
import { AddBookmarkComponent } from './components/add-bookmark/add-bookmark.component';
import { UpdateBookmarkComponent } from './components/update-bookmark/update-bookmark.component';

import { BookmarkService } from './services/bookmark.service';
import { NotificationService } from './services/notification.service';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
	    MatButtonModule,
	    MatDialogModule,
	    MatIconModule,
	    MatInputModule,
	    MatListModule,
	    MatSidenavModule,
	    MatSnackBarModule,
		MatFormFieldModule,
	    MatTableModule,
	    MatToolbarModule,
	    MatTooltipModule,
        FormsModule,
        EffectsModule.forRoot(effects),
        StoreModule.forRoot(reducers, {initialState}),
        StoreDevtoolsModule.instrument( {maxAge: 30} ),
    ],
    declarations: [
        AppComponent,
        BookmarkComponent,
        PopupComponent,
        MessageComponent,
        BmTableComponent,
        AddBookmarkComponent,
        UpdateBookmarkComponent
    ],
    providers: [
	    BookmarkService, NotificationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }