import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';

interface NotificationConfig {
	duration: number
	vPos: MatSnackBarVerticalPosition
    hPos: MatSnackBarHorizontalPosition
    message: string
}

@Injectable({providedIn: 'root'})
export class NotificationService {
	constructor(private snackBar: MatSnackBar) {
	    
    }

    showNotification (config : NotificationConfig) {
		this.snackBar.open(config.message, null, {
            duration: config.duration,
            verticalPosition: config.vPos,
            horizontalPosition: config.hPos
        });
    }
}
