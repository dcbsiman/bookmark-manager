import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bookmarks } from '../store/state/bookmark.state';

@Injectable({providedIn: 'root'})
export class BookmarkService {

    private dummyBookmarks: Bookmarks = {
        bookmarks: [
            {id:1, name: 'LinkedIn', url: 'https://www.linkedin.com/', group: 'Work'},
            {id:2, name: 'Jobstreet', url: 'http://www.jobstreet.com.ph/', group: 'Work'},
            {id:3, name: 'Cebu Pacific', url: 'https://www.cebupacificair.com/', group: 'Leisure'},
            {id:4, name: 'Facebook', url: 'https://facebook.com/', group: 'Personal'},
            {id:5, name: 'Twitter', url: 'https://twitter.com/', group: 'Personal'}
        ]
    };

    getBookmarks(): Observable<Bookmarks>{
        return new Observable(observer => {
            observer.next(this.dummyBookmarks);
            observer.complete();
        });
    }
}
