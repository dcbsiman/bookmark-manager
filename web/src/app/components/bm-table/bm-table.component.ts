import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { NotificationService } from '../../services/notification.service';

interface BmTableConfig {
	columns: any[]
	displayedColumns: string[]
    tooltip: any
    data: any
    editPopupConfig: any
    deletePopupConfig: any
    deleteFunc: Function
}

@Component({
	selector: 'bm-table',
    styleUrls: ['bm-table.component.scss'],
    templateUrl: 'bm-table.component.html',
})
export class BmTableComponent {
	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    @Input() config : BmTableConfig;

	delete(data: any) {
		this.config.deleteFunc(data, this.store, this.notificationService);
	}
}
