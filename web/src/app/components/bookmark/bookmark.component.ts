import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AddBookmarkComponent } from '../add-bookmark/add-bookmark.component';
import { UpdateBookmarkComponent } from '../update-bookmark/update-bookmark.component';
import { AppState, selectBookmarks, selectGroup, selectBookmarksByGroup } from '../../store/state/app.state'; 
import { Bookmark } from '../../store/state/bookmark.state';
import { LoadBookmarkInit, DeleteBookmark } from '../../store/actions/bookmark.actions';
import { NotificationService } from '../../services/notification.service';

@Component({
    selector: 'bookmark',
    styleUrls: ['bookmark.component.scss'],
    templateUrl: 'bookmark.component.html',
})
export class BookmarkComponent {
    bookmarkTableConfig;
    addPopupConfig;
    activeFolder;
    groups$: Observable<string[]>;
    groups: string[];
    bookmarks$: Observable<Bookmark[]>;
    bookmarks: Bookmark[];
    selectedGroup: string;
	isEditMode = false;
    constructor(private store: Store<AppState>) {

    }

	ngOnInit() {
	    this.addPopupConfig = {
			type: 'template',
			attribute: 'mat-mini-fab',
			fontIconColor: 'primary',
			fontIcon:'add',
			tooltip:'Add Bookmark',
			template: AddBookmarkComponent
		}

		this.loadData();
		
		this.bookmarkTableConfig = {
			data: this.bookmarks,
			displayedColumns: ['name', 'url', 'group', 'action'],
			columns: [{
				header: "Name",
				field: "name"
			},{
				header: "URL",
				field: "url"
			},{
				header: "Group",
				field: "group"
			}],
			editPopupConfig: {
				type: 'template',
				attribute: 'mat-icon-button',
				fontIconColor: 'primary',
				fontIcon:'edit',
				tooltip:'Edit Bookmark',
				template: UpdateBookmarkComponent
			},
			deletePopupConfig: {
				type: 'message',
				attribute: 'mat-icon-button',
				fontIconColor: 'warn',
				fontIcon:'delete',
				tooltip:'Delete Bookmark',
				messageConfig: {
				    message: 'Are you sure you want to remove this bookmark ?',
                    primaryButtonEnabled: true,
                    primaryButtonLabel: 'Yes',
                    secondaryButtonEnabled: true,
                    secondaryButtonLabel: 'No',
                    primaryButtonCallback: function(store: Store<AppState>, notificationService: NotificationService, data: any) {
	                    store.dispatch(new DeleteBookmark(data));
					    notificationService.showNotification({
						    duration: 2000,
						    vPos: 'top',
						    hPos: 'center',
						    message: 'Bookmark was deleted successfully.'
					    });
                    }
				}
			}
		}
	}

	isActive(group: string) {
	    return group === this.activeFolder;
	}

    private loadData() {
	    this.groups$ = this.store.pipe(select(selectGroup));
        this.groups$.subscribe(response => {
		    this.groups=response;
        });

        this.loadGroup('All');
        this.activeFolder='All';
        this.loadBookmarks();
    }

    loadGroup(group : string) {
	    this.selectedGroup = group;
        if(group === 'All') {
	        this.bookmarks$ = this.store.pipe(select(selectBookmarks));
        } else {
	        this.bookmarks$ = this.store.pipe(select(selectBookmarksByGroup(group)));
        }

        this.bookmarks$.subscribe(response => {
		    this.bookmarks=response;

			if(this.bookmarkTableConfig) {
				this.bookmarkTableConfig.data=this.bookmarks;
			}
        });

        this.activeFolder = group;
    }

    private loadBookmarks() {
	    this.store.dispatch(new LoadBookmarkInit(null));
    }
}
