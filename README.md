# Bookmark Manager

---
Service exists here:
https://bitbucket.org/dcbsiman/bookmark-manager
---

## Screenshots

#### Overview
![Alt text](screenshots/a1-overview.PNG)
#### Groups
![Alt text](screenshots/a2-group.PNG)
#### Others
![Alt text](screenshots/a3-actions.PNG)
![Alt text](screenshots/a5-addbutton.PNG)
#### Dialogs
![Alt text](screenshots/a4-add.PNG)
![Alt text](screenshots/a6-update.PNG)

## How to run the application

- Checkout/Download bitbucket repository https://bitbucket.org/dcbsiman/bookmark-manager
- Go to the root folder and run: 
```sh
npm install
ng serve
```

 - Navigate to http://localhost:8080/. The app will automatically reload if you change any of the source files.

## Functionality

1. Add bookmarks
1. Edit bookmarks.
1. Delete bookmarks.
1. View all bookmarks
1. View the bookmarks grouped by a group.
 
---
## Description of the solution

### Folder structure & files
![Alt text](screenshots/a7-structure.PNG)



- **src/app/bookmark/state**: contains all the components that implement Redux inspired NgRx pattern for managing the state of the bookmark.

- **src/app/app.state.ts**: contains the state (store) of the whole application (currently only bookmark's).

- **src/app/services/bookmark.service.ts**: contains a mock service that returns some predefined bookmarks.

### NgRx Actions
- **GET_BOOKMARKS** = '[Bookmark] Get Bookmarks',
- **ADD_BOOKMARK**= '[Bookmark] Add Bookmark',
- **UPDATE_BOOKMARK**= '[Bookmark] Update Bookmark',
- **DELETE_BOOKMARK** = '[Bookmark] Delete Bookmark',
- **LOAD_BOOKMARK_INIT** = '[Bookmark] Load Bookmark init',
- **LOAD_BOOKMARK_DONE** = '[Bookmark] Load Bookmark done',
- **RESTORE_BOOKMARKS** = '[Bookmark] Restore Bookmarks to current state'

### NgRx Reducers
The reducer "listens" to the following actions, apart from the default one:

- **GET_BOOKMARKS**: returns the state with all bookmarks
- **CREATE_BOOKMARK**: adds the new bookmark to the state and returns the updated state
- **UPDATE_BOOKMARK**: replaces the edited bookmark to the state and returns the updated state
- **LOAD_BOOKMARK_DONE**:  adds the new bookmarks that were created by **bookmark.service.ts -> getBookmarks()** to the state and returns the updated state
- **RESTORE_BOOKMARKS**: it is not handled by the reducer, so the default case is executed. It was used in order to return the existing state after the action call, if needed.

### NgRx Effects

- **loadBookmarks**: It is triggered by **LOAD_BOOKMARK_INIT** actions & it calls **LOAD_BOOKMARK_DONE**.
> As there is no error handling, the postfix _DONE was used instead of _SUCCESS for the LOAD_BOOKMARK_DONE action

### NgRx Selectors 
- **selectBookmarks**: returns an observable with all bookmarks
- **selectBookmarksByGroup**: returns an observable with the bookmarks of the group that was passed as a parameter.

### NgRx States
- **interfaces**: Bookmarks, Bookmark
- **initialStates**: initialBookmarksState

## Initial data
As a showcase initial data are loaded as follows:
- by calling **LOAD_BOOKMARK_INIT** action in **src/app/bookmark/bookmark.component.ts** -> ngOnInit(). That action triggers an effect and the **Bookmark Manager** is called.
> That is the only case that an effect is being called. In all other cases state modified directly in the reducer.

## Node and NPM Versions
- **NodeJS**: v13.9.0
- **NPM**: 6.13.7